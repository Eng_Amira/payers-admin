require 'rails_helper'

RSpec.describe "nationalid_verifications/new", type: :view do
  before(:each) do
    assign(:nationalid_verification, NationalidVerification.new(
      :user_id => 1,
      :legal_name => "MyString",
      :national_id => 1,
      :type => 1,
      :status => 1,
      :note => "MyString"
    ))
  end

  it "renders new nationalid_verification form" do
    render

    assert_select "form[action=?][method=?]", nationalid_verifications_path, "post" do

      assert_select "input[name=?]", "nationalid_verification[user_id]"

      assert_select "input[name=?]", "nationalid_verification[legal_name]"

      assert_select "input[name=?]", "nationalid_verification[national_id]"

      assert_select "input[name=?]", "nationalid_verification[type]"

      assert_select "input[name=?]", "nationalid_verification[status]"

      assert_select "input[name=?]", "nationalid_verification[note]"
    end
  end
end
