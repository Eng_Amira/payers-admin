require 'rails_helper'

RSpec.describe "admin_watchdogs/index", type: :view do
  before(:each) do
    assign(:admin_watchdogs, [
      AdminWatchdog.create!(
        :admin_id => 2,
        :ipaddress => "MyText",
        :operation_type => "Operation Type"
      ),
      AdminWatchdog.create!(
        :admin_id => 2,
        :ipaddress => "MyText",
        :operation_type => "Operation Type"
      )
    ])
  end

  it "renders a list of admin_watchdogs" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Operation Type".to_s, :count => 2
  end
end
