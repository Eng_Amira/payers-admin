require 'rails_helper'

RSpec.describe "admin_logins/show", type: :view do
  before(:each) do
    @admin_login = assign(:admin_login, AdminLogin.create!(
      :admin => nil,
      :ip_address => "Ip Address",
      :user_agent => "User Agent",
      :device_id => "Device",
      :operation_type => "Operation Type"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Ip Address/)
    expect(rendered).to match(/User Agent/)
    expect(rendered).to match(/Device/)
    expect(rendered).to match(/Operation Type/)
  end
end
