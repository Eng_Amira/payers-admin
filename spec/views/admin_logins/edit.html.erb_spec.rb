require 'rails_helper'

RSpec.describe "admin_logins/edit", type: :view do
  before(:each) do
    @admin_login = assign(:admin_login, AdminLogin.create!(
      :admin => nil,
      :ip_address => "MyString",
      :user_agent => "MyString",
      :device_id => "MyString",
      :operation_type => "MyString"
    ))
  end

  it "renders the edit admin_login form" do
    render

    assert_select "form[action=?][method=?]", admin_login_path(@admin_login), "post" do

      assert_select "input[name=?]", "admin_login[admin_id]"

      assert_select "input[name=?]", "admin_login[ip_address]"

      assert_select "input[name=?]", "admin_login[user_agent]"

      assert_select "input[name=?]", "admin_login[device_id]"

      assert_select "input[name=?]", "admin_login[operation_type]"
    end
  end
end
