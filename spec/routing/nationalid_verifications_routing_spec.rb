require "rails_helper"

RSpec.describe NationalidVerificationsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/nationalid_verifications").to route_to("nationalid_verifications#index")
    end

    it "routes to #new" do
      expect(:get => "/nationalid_verifications/new").to route_to("nationalid_verifications#new")
    end

    it "routes to #show" do
      expect(:get => "/nationalid_verifications/1").to route_to("nationalid_verifications#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/nationalid_verifications/1/edit").to route_to("nationalid_verifications#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/nationalid_verifications").to route_to("nationalid_verifications#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/nationalid_verifications/1").to route_to("nationalid_verifications#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/nationalid_verifications/1").to route_to("nationalid_verifications#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/nationalid_verifications/1").to route_to("nationalid_verifications#destroy", :id => "1")
    end
  end
end
