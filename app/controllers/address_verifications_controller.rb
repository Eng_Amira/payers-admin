class AddressVerificationsController < ApplicationController
  before_action :set_address_verification, only: [:show, :edit, :update, :destroy]
  before_action :require_login


  # show list of all Address Verifications,
  # @return [String] UserName
  # @return [String] Country
  # @return [String] City
  # @return [String] State
  # @return [String] Street
  # @return [String] apartment_number
  # @return [String] Status
  # @return [String] Note
  def index
    if current_user.roleid == 1 
        @address_verifications = AddressVerification.all
    else
        redirect_to root_path , notice: "not allowed" 
    end
  end

  # show details of Address Verification of specific user
  # @param [Integer] id 
  # @return [String] UserName
  # @return [String] Country
  # @return [String] City
  # @return [String] State
  # @return [String] Street
  # @return [String] apartment_number
  # @return [String] Status
  # @return [String] Note 
  # @return [String] attachment 
  
  def show
    if current_user.roleid != 1  
      redirect_to root_path , notice: "not allowed" 
    end
  end

  
  # edit Address Verification
  # admin can edit address verification only if status is not verified
  # @param [Integer] id
  def edit  
    if (@address_verifications.status == "Verified") 
      redirect_to root_path , notice: "not allowed" 
    end  
  end

  
  # edit Address Verification,
  # only admins can edit status and note.
  # @param [Integer] country_id
  # @param [String] city
  # @param [String] state
  # @param [String] street
  # @param [String] apartment_number
  # @param [Blob] attachment
  # @param [Integer] status
  # @param [String] note
  # @return [Integer] id
  # @return [String] username
  # @return [Integer] country_id
  # @return [String] state
  # @return [String] street
  # @return [String] apartment_number
  # @return [Blob] attachment
  # @return [Integer] status
  # @return [String] note
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def update
    respond_to do |format|
      if @address_verification.update(address_verification_params)
        if current_user.roleid == 1
          @userinfo = UserInfo.where("user_id =?", @address_verification.user_id ).first
          if params[:address_verification][:status] == "Verified"
             if @userinfo == nil              
                 UserInfo.create(:user_id => @address_verification.user_id , :address_verification_id => @address_verification.id, :nationalid_verification_id => 0, :selfie_verification_id => 0, :status => "UnVerified")
             else
              @userinfo.update(:address_verification_id => @address_verification.id)
              @user_nationalid_verification = NationalidVerification.where("user_id =?", @address_verification.user_id ).first
              if @user_nationalid_verification != nil and @user_nationalid_verification.status == "Verified"
                 @user_selfie_verification = SelfieVerification.where("user_id =?", @address_verification.user_id ).first
                 if @user_selfie_verification != nil and @user_selfie_verification.status == "Verified"
                  @userinfo.update(:status => "Verified")
                 end
              end
             end
          elsif params[:address_verification][:status] == "UnVerified" and @userinfo != nil
              @userinfo.update(:status => "UnVerified")
          end
        end
        format.html { redirect_to @address_verification, notice: 'Address verification was successfully updated.' }
        format.json { render :show, status: :ok, location: @address_verification }
      else
        format.html { render :edit }
        format.json { render json: @address_verification.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_address_verification
      @address_verification = AddressVerification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def address_verification_params
      params.require(:address_verification).permit(:user_id, :country_id, :city, :state, :street, :apartment_number, :status, :note, :attachment)
    end
end
