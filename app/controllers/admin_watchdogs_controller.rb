class AdminWatchdogsController < ApplicationController
  before_action :set_admin_watchdog, only: [:show, :edit, :update, :destroy]

  # GET /admin_watchdogs
  # GET /admin_watchdogs.json
  def index
    @admin_watchdogs = AdminWatchdog.all
  end

  # GET /admin_watchdogs/1
  # GET /admin_watchdogs/1.json
  def show
  end


  # show log of specific admin
  # works only if current user is an admin
  # @return [String] username
  # @return [String] ipaddress	
  # @return [String] details
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def admin_log
    if current_user.roleid == 1 or current_user.id == params[:id].to_i
      @log = AdminWatchdog.where("admin_id =? ",params[:id]).joins("INNER JOIN admins ON admins.id = admin_watchdogs.admin_id").distinct.all.order('created_at DESC')
    else
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # GET /admin_watchdogs/new
  def new
    @admin_watchdog = AdminWatchdog.new
  end

  # GET /admin_watchdogs/1/edit
  def edit
  end

  # POST /admin_watchdogs
  # POST /admin_watchdogs.json
  def create
    @admin_watchdog = AdminWatchdog.new(admin_watchdog_params)

    respond_to do |format|
      if @admin_watchdog.save
        format.html { redirect_to @admin_watchdog, notice: 'Admin watchdog was successfully created.' }
        format.json { render :show, status: :created, location: @admin_watchdog }
      else
        format.html { render :new }
        format.json { render json: @admin_watchdog.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin_watchdogs/1
  # PATCH/PUT /admin_watchdogs/1.json
  def update
    respond_to do |format|
      if @admin_watchdog.update(admin_watchdog_params)
        format.html { redirect_to @admin_watchdog, notice: 'Admin watchdog was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_watchdog }
      else
        format.html { render :edit }
        format.json { render json: @admin_watchdog.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin_watchdogs/1
  # DELETE /admin_watchdogs/1.json
  def destroy
    @admin_watchdog.destroy
    respond_to do |format|
      format.html { redirect_to admin_watchdogs_url, notice: 'Admin watchdog was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_watchdog
      @admin_watchdog = AdminWatchdog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_watchdog_params
      params.require(:admin_watchdog).permit(:admin_id, :logintime, :ipaddress, :lastvisit, :operation_type)
    end
end
