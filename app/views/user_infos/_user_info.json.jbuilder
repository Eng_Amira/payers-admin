json.extract! user_info, :id, :user_id, :mobile, :address_verification_id, :nationalid_verification_id, :nationalid_verification_id, :selfie_verification, :status, :created_at, :updated_at
json.url user_info_url(user_info, format: :json)
