Rails.application.routes.draw do



  resources :admin_watchdogs
  resources :admin_logins
  resources :selfie_verifications,:except => [:new, :create]
  resources :address_verifications,:except => [:new, :create]
  resources :nationalid_verifications,:except => [:new, :create]
  resources :user_infos
  resources :user_verifications
  resources :user_verfications
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :watchdogs
      resources :users
      resources :countries
      resources :affilate_programs
      post "users/create" => "users#create"
      get "homepage" => "users#homepage"
      post "confirmmail" => "users#confirmmail"
      post "resend_confirmmail" => "users#resend_confirmmail"
      get "two_factor" => "users#get_two_factor"
      post "two_factor" => "users#post_two_factor"
      get "statistics" => "users#statistics"
      get "lock_unlock_account" => "users#lock_unlock_account"
      get "send_confirmation_email" => "users#send_confirmation_email"
      get "change_confirmation_code" => "users#change_confirmation_code"
      get "confirm_google_code" => "users#confirm_google_code"
      get "set_otp_code" => "users#set_otp_code"
      get "user_log" => "watchdogs#user_log"
      get "active_sessions" => "logins#active_sessions"
      post 'auth_user' => 'authentication#authenticate_user'
      post "unlockaccount" => "authentication#unlockaccount"
      post "send_invitation_email" => "affilate_programs#send_invitation_email"
      post 'getEditToken' => 'authentication#getEditToken'
    end
  end


  resources :admins,:except => [:new]
  resources :affilate_programs
  resources :countries
  resources :logins
  resources :verifications
  resources :watchdogs
  resources :users,:except => [:new]
  root "admins#homepage"
  get "confirmmail" => "users#confirmmail"
  get "unlockaccount" => "admins#unlockaccount"
  get "two_factor" => "admins#get_two_factor"
  post "two_factor" => "admins#post_two_factor"
  get "user_log" => "watchdogs#user_log"
  get "admin_log" => "admin_watchdogs#admin_log"
  get "statistics" => "admins#statistics"
  get "lock_unlock_account" => "users#lock_unlock_account"
  get "active_sessions" => "logins#active_sessions"
  get "admin_active_sessions" => "admin_logins#admin_active_sessions"
  get "send_confirmation_email" => "admins#send_confirmation_email"
  get "change_confirmation_code" => "admins#change_confirmation_code"
  get "confirm_google_code" => "admins#confirm_google_code"
  #get "affilate-program/:refered_by" => "affilate_programs#send_invitation"
  #post "send_invitation_email" => "affilate_programs#send_invitation_email"
  #get "edit_password/:id" => "users#edit_password" , :as => "edit_password" 
  get "add_admin" => "admins#add_admin"
  post "add_admin" => "admins#add_admin_post"
  get "error_page" => "logins#error_page"
  get "lock_page" => "admin_logins#lock_page"
  post "lock_page" => "admin_logins#check_to_unlock"


  resources :passwords,
      controller: 'clearance/passwords',
      only: [:create, :new]

  resource :session,
      controller: 'clearance/sessions',
      only: [:create]

  resources :admins,
      controller: 'admins',
      only: Clearance.configuration.user_actions do
        resource :password,
          controller: 'clearance/passwords',
          only: [:create, :edit, :update]
      end

  get '/sign_in' => 'clearance/sessions#new', as: 'sign_in'
  delete '/sign_out' => 'clearance/sessions#destroy', as: 'sign_out'

  if Clearance.configuration.allow_sign_up?
   get '/sign_up' => 'admins#new', as: 'sign_up'
  end
  



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
