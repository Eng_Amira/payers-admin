class ChangePhoneCodeToBeStringInCountries < ActiveRecord::Migration[5.2]
  def change
    change_column :countries, :Phone_code, :string, :limit => 10
  end
end
