class CreateLogins < ActiveRecord::Migration[5.2]
  def change
    create_table :logins do |t|
      t.belongs_to :user, foreign_key: true
      t.string :ip_address
      t.string :user_agent
      t.string :device_id

      t.timestamps
    end
    add_index :logins, :device_id
  end
end
