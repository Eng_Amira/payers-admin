class AddValueToOtpSecret < ActiveRecord::Migration[5.2]
  def change
    User.all.each { |user| user.update_attribute(:otp_secret_key, ROTP::Base32.random_base32) }

  end
end
