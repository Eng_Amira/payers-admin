class CreateAdminLogins < ActiveRecord::Migration[5.2]
  def change
    create_table :admin_logins do |t|
      t.belongs_to :admin, foreign_key: true
      t.string :ip_address
      t.string :user_agent
      t.string :device_id
      t.string :operation_type

      t.timestamps
    end
  end
end
